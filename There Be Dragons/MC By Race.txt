Dwarf, Constitution 12
Medium Size, Speed 5, Armor Class 10
poisons +2

Elf, Dexterity 12
Medium Size, Speed 6, Armor Class 10
Perception +2, Investigate +2
enchantments +2

Halfling, Dexterity 12
Small Size, Speed 5, Armor Class 11
Athletics +2, Stealth +2
fear effects +2

Human
Medium Size, Speed 6, Armor Class 10
Any One Feat
Any One Skill

Gnome, Intelligence 12
Small Size, Speed 5, Armor Class 11
Perception +2, Stealth +2
illusions +2
