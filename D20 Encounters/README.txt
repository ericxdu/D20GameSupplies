A Note About Monster Encounters

Even predatory creatures in Real Life more often choose to preserve
their life rather than fight to the death. Monsters, on the other hand,
are creatures that will attack on sight. Motivated by hunger or
territory, monsters are meant to be terrifying creatures that will
fight tooth and nail until dead. Creative game masters may find
another way to spin creature encounters, but this is the default.