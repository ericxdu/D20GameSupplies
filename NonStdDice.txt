1D2

Roll any even-sided die. If the result is an even number, read it as
2. If the result is an odd number, read it as 1.


1D3

Roll a D6. If the die shows > 3, simply check the face down side for
your result, or use the following formula: 4 is 3, 5 is 2, 6 is 1.


1-12
    
Roll a D6 and read any result of 6 as a 0. Reroll the die and read any
result of 1 as a 7. Add the second result to the first result.
